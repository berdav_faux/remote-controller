// Testing environment.
process.env.NODE_ENV = "test";

// Load chai plugins.
const chai = require("chai");
chai.use(require("chai-as-promised"));
