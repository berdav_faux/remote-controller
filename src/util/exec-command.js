const childProcess = require("child_process");
const util = require("util");

const exec = util.promisify(childProcess.exec);

async function execSaltCommand(machineId, command) {
  return await exec(`salt '${machineId}' cmd.run '${command}'`);
}

async function execDummyCommand(machineId, command) {
  return {
    stdout: `Command (machine: ${machineId}) => ${command}`,
    stderr: ""
  };
}

module.exports.execSaltCommand = execSaltCommand;
module.exports.execDummyCommand = execDummyCommand;
