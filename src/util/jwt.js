const { promisify } = require("util");

const jwt = require("jsonwebtoken");

const jwtSign = promisify(jwt.sign);
const jwtVerify = async function(s,p) {
	return await jwt.verify(s,p);
}

async function jwtGenerateAuthToken(secret, signPK) {
  return await jwtSign({ secret }, signPK, { algorithm: "RS256" });
}

module.exports.jwtVerify = jwtVerify;
module.exports.jwtGenerateAuthToken = jwtGenerateAuthToken;
