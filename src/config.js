const path = require("path");
const https = require('https');
const fs = require('fs');

const cryptoRandomString = require("crypto-random-string");

const rootDir = path.join(path.dirname(__dirname));
const testDataDir = path.join(rootDir, "data-test");

config = {
  listenPort: 3333,
  pepper: cryptoRandomString(32),
  controller: {
    pm: {
      machineIdRegexp: /obs-tec-[a-z]{2,}\d{2}/
    }
  },
  twoWaySSL: { server: {}, client: {} },
  jwt: { server: {}, client: {} },
  secret: {},
  corsOptions: {}
};

if (process.env.NODE_ENV === "test" || process.env.NODE_ENV === "development") {
  config.twoWaySSL.server.pkPath = path.join(testDataDir, "server-pk.pem");
  config.twoWaySSL.server.certPath = path.join(testDataDir, "server-cert.pem");

  config.jwt.server.pkPath = path.join(testDataDir, "jwt-server-pk.pem");
  config.jwt.server.certPath = path.join(testDataDir, "jwt-server-cert.pem");

  config.jwt.client.pkPath = path.join(testDataDir, "jwt-client-pk.pem");
  config.jwt.client.certPath = path.join(testDataDir, "jwt-client-cert.pem");

  config.secret.method = "dummy";
  config.secret.value = "1234";

  config.controller.pm.execStrategy = "dummy";
}

if (process.env.NODE_ENV === "production") {
  const trustedCa = [
    process.env.NODE_EXTRA_CA_CERTS
  ];

  https.globalAgent.options.ca = [];
  for (const ca of trustedCa) {
    https.globalAgent.options.ca.push(fs.readFileSync(ca));
  }

  config.twoWaySSL.server.pkPath = process.env.SSL_SERVER_PK_PATH;
  config.twoWaySSL.server.certPath = process.env.SSL_SERVER_CERT_PATH;

  config.jwt.server.pkPath = process.env.JWT_SERVER_PK_PATH;
  config.jwt.server.certPath = process.env.JWT_SERVER_CERT_PATH;

  config.jwt.client.pkPath = null;
  config.jwt.client.certPath = process.env.JWT_CLIENT_CERT_PATH;

  config.secret.method = "vault";
  config.secret.path = "{{pillar.todo}}";
  config.secret.endpoint = "{{pillar.todo}}";
  config.secret.apiVersion = "v1";
  config.secret.token = "/etc/remote-controller/token";
  config.secret.token_key = "remote-controller-token";

  config.controller.pm.execStrategy = "saltstack";

  config.corsOptions.origin = "{{pillar.todo}}";
  config.corsOptions.methods = 'POST';
  config.corsOptions.exposedHeaders = 'Authorization';
  config.corsOptions.allowedHeaders = 'Authorization, kbn-version';
  config.corsOptions.credentials = true;
}

module.exports = config;
